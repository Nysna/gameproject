import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import './style/index.css';
import Register from "./components/Register";
import Connection from './components/Connection';
import Home from "./components/Home";

export default function App() {
    return (
        <Router>
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/connection">Connection</Link>
                        </li>
                    </ul>
                </nav>

                <Switch>
                    <Route path="/register">
                        <Register />
                    </Route>
                    <Route path="/connection">
                        <Connection />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}
