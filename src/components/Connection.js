import React from 'react';
import {
    Link
} from "react-router-dom";

class Connection extends React.Component {
    render() {
        return (
            <div>
                <h1>CONNECTION PAGE</h1>
                <nav>
                <h3>Not yet registered ? </h3>
                <ul>
                    <li>
                        <Link to="/register">Register</Link>
                    </li>
                </ul>
            </nav>
            </div>
        );
    }
}

export default Connection;
