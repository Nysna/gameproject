import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import App from './main';

ReactDOM.render(
    <App />, document.getElementById('root')
);
